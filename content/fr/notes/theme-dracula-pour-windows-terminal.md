---
title: Thème "Dracula" pour Windows Terminal
description: 'Thème "Dracula" pour Windows Terminal'
category: Notes
createdAt: 2020-07-05
keywords: thème dracula "windows terminal"
position: 10
---

Avoir les mêmes codes couleur quelque soit l'application, qu'elle soit une application web, un client lourd voir un terminal, ça peut être intéressant.

Après le [thème dracula pour GameMaker](/divers/theme-dracula-pour-gms-2), vous serez donc content (comme moi) d’apprendre que ce thème existe aussi pour le tout frais Windows Terminal.

![Aperçu du thème dracula](https://draculatheme.com/static/img/screenshots/windows-terminal.png)

Pour le télécharger et l’installer, ça se passe par [là](https://draculatheme.com/windows-terminal/) mais son installation est si simple qu'il n'est même pas besoin de télécharger quoi que ce soit.

Le thème "Dracula" est disponible pour une pléthore d'applications, Notepad++ notamment que j'utilise toujours pour lire certains fichiers.