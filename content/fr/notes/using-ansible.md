---
title: Ansible
description: "Mémo sur ansible"
category: Notes
createdAt: 2019-09-19
keywords: ansible python python3 pywinrm powershell windows
position: 1
---

## Introduction



## Création d'un rôle

ansible-galaxy init create_user

ls -lR

pip3 list --format=columns | grep netaddr

## Sorties ansible

Pour modifier les éléments affichés par ansible lors d'un traitement, il faut éditer le fichier de configuration.

```
# on élude les hôtes non traités
display_skipeed_hosts = no
# pas de couleur
nocolor=1
# voir temps d'exécution
callback_whitelist = profile_tasks
# sortie ligne par ligne
stdout_callback = minimal # ou online / null
```

## envoi des logs vers rsyslog

Toujours dans le fichier de configuration.

```
callback_whitelist = syslog_json
[callback_syslog_json]
syslog_server = localhost # ou une autre adresse
```

Pour voir les logs d'ansible : 

`tail /var/log/messages`