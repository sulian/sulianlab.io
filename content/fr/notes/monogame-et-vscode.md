---
title: Utiliser Monogame avec VSCode
description: Utliser le framework Monogame avec VSCode
category: Notes
createdAt: 2021-12-11
keywords: monogame vscode c#
position: 7
---

## Introduction

Il n'est pas obligatoire d'utiliser Visual Studio Community Edition pour développer des jeux avec le framework open-cource [Monogame](https://www.monogame.net/).

En effet, le framework .Net et Monogame sont accessible depuis le terminal et il est donc possible d'exécuter des commandes depuis VS Code.

## Extension C# pour VS Code

Pour utiliser le langage C# dans VS Code, il est conseillé d'installer l'extension [C# for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp). Cette extension est basée sur OmniSharp, un projet Open Source permettant d'utiliser .NET avec n'importe quelle éditeur de code (VS Code, Atom, Vim).

## Exécuter, compiler et déboger C# dans VS Code

### Création d'un nouveau projet

```
dotnet new console -o App1

cd App1

```

### Lancement de VS Code

`code .`

### Exécuter le code

Pour exécuter le projet, on passe par .NET Core en ligne de commande :

`dotnet run`

## Déboger le code

Grâce à l'extension C# et en utilisant la palette de commande (`Ctrl+Maj+P`), il est possible de préparer le projet à la compilation et à l'exécution :

![](/vscode/vscode-csharp-01.png)

Il est maintenant possible de procéder au débogage du projet si nécessaire :

![](/vscode/vscode-csharp-02.png)

### Compiler le code

Toujorus avec .NET Core en ligne de commande :

`dotnet build`

## Installation du framework .Net

Monogame repose sur le langage C#, il faut donc d'abord installer le framework .NET. Contrairement à Visual Studio Community Edition, nous allons installer uniquement les composants nécessaires pour compiler les projets écrits avec le framework Monogame.

1. .NET SDK 5.x
1. .NET Core SDK 3.x
1. .NET Runtime 5.x

Malgré la sortie de .NET version 6, il est conseillé d'installer la version 5 du framework. Tous les téléchargements sont disponibles sur le [site .NET de Microsoft](https://dotnet.microsoft.com/en-us/download) ou directement [ici](https://dotnet.microsoft.com/en-us/download/dotnet/5.0).

Après installation, vous pouvez lancer une fenêtre du terminal et saisir `dotnet` pour vérifier que .NET est bien installé :

![](/vscode/monogame-01.png)

<alert type="warning">

Il faut télécharger la version 64 bits du framework .NET, le framework Monogame ne fonctionne pas avec une version 32 bits.

</alert>

### Désactivation de la télémétrie

Comme beaucoup d'outils de Microsoft, le .NET Core collecte aussi des données d'usage, il est possible de ésactiver la collecte des données comme suit (de manière définitive):

`setx DOTNET_CLI_TELEMETRY_OPTOUT 1` 

## Installation du framework MonoGame

Certains utilitaires sont requis pour pouvoir utiliser le framework MonoGame notamment l'éditeur MGCB qui servira à gérer les ressources d'un projet.

```
dotnet tool install --global dotnet-mgcb-editor
```

Dans certains cas, il faudra préciser la version du paquet :

```
dotnet tool install --global dotnet-mgcb-editor --version 3.8.0.1641
```

Si l'installation échoue, cela peut provenir d'une mauvaise configuration de Nuget. Il faut alors vérifier sa configuration.

Dans `%USERPROFILE%\AppData\Roaming\NuGet\nuget.config`, vérifiez que la source des paquets est correctement renseignée :

```
<packageSources>
    <add key="nuget.org" value="https://api.nuget.org/v3/index.json" />
</packageSources>
```

Il faut ensuite enregistrer l'application MGCB :

`mgcb-editor --register`

Enfin, il faut installer les différents modèles :

`dotnet new --install MonoGame.Templates.CSharp`

Ici aussi, il faudra préciser la la version des modèles en cas de problème :

`dotnet new --install MonoGame.Templates.CSharp::3.8.0.1641`

## Création d'un nouveau projet

On utilisera le modèle *mgdesktopgl* qui permet de faire des jeux multi-plateformes.

```
dotnet new mgdesktopgl -o ProjectName

cd ProjectName

dotnet add package MonoGame.Framework.DesktopGL --version 3.8.0.1641
```

Lancement de VS Code

`code .`

Test du programme

`dotnet run Program.cs`

![](/vscode/monogame-02.png)

## Import d'un projet existant

Il est possible de récupérer sur le [github de Monogame](https://github.com/MonoGame/MonoGame.Samples) plusieurs exemples de jeu multi-plaforme. Ouvrir un de ces projets dans VS Code est une bonne manière de tester votre configuration et voir si tout fonctionne :

![](/vscode/monogame-demo.gif)


## Sources

- [How to Run C# in VSCode](https://travis.media/how-to-run-csharp-in-vscode/)
- [Monogame documentation](https://docs.monogame.net/index.html)
- [Getting Started with C# MonoGame in VS code](https://medium.com/learning-c-by-developing-games/getting-started-with-c-monogame-in-vs-code-2c26c7f198c2)