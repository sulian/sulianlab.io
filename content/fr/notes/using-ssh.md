---
title: SSH
description: "Notes sur SSH"
category: Notes
createdAt: 2024-01-24
slug: using-ssh
keywords: ssh, git, github, gitlab
position: 11
---

# Utilisation de SSH




### Copy SSH key to Clipboard

#### Mac

`pbcopy < ~/.ssh/id_rsa.pub`

#### Linux (Ubuntu)

`cat ~/.ssh/id_rsa.pub`

#### Windows (Git Bash)

`clip < ~/.ssh/id_rsa.pub`

## Liens

* [Connexion à GitHub à l’aide de SSH](https://docs.github.com/fr/authentication/connecting-to-github-with-ssh)
* [Gestion des clés de déploiement](https://docs.github.com/fr/authentication/connecting-to-github-with-ssh/managing-deploy-keys)
* [Génération d’une nouvelle clé SSH et ajout de celle-ci à ssh-agent](https://docs.github.com/fr/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)
* [Ajout d’une nouvelle clé SSH à votre compte GitHub](https://docs.github.com/fr/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account)
* [Vérification des clés SSH existantes](https://docs.github.com/fr/authentication/connecting-to-github-with-ssh/checking-for-existing-ssh-keys)
* [Setting your commit email address](https://docs.github.com/en/account-and-profile/setting-up-and-managing-your-personal-account-on-github/managing-email-preferences/setting-your-commit-email-address)

