---
title: PowerShell
description: "Mémo sur powershell"
category: Notes
createdAt: 2021-03-10
keywords: powershell windows
position: 8
---

## Introduction


## Paramètres

Pour connaitre la version installée :

    $PSVersionTable.PSVersion

Pour connaitre la version de Windows :

    [System.Environment]::OSVersion.Version

Pour obtenir les divers emplacements pour l'enregistrement des modules :

    $Env:PSModulePath

### Utilisateur courant

**Méthode 1**

    [Environment]::UserName

**Méthode 2**

    [System.Security.Principal.WindowsIdentity]::GetCurrent().Name

**Méthode 3**

    whoami

## Configuration

### Autoriser l'exécution des scripts

**Solution 1 :** sur tous les postes

    Set-ExecutionPolicy -ExecutionPolicy Unrestricted

**Solution 2 :** utilisation d'une GPO

Configurer l'exécution depuis `Configuration ordinateur` →
`Composants Windows` → `Windows PowerShell` →
`Activer l'exécution des scripts` : `Autoriser tous les scripts`.

Pour vérifier la bonne application des paramètres :

    Get-ExecutionPolicy

## Utilisation

<d-alert>

Dans un script, on peut nettoyer l'affichage avec `Clear-Host` qui est l'équivalent de `cls`. 

</d-alert>

Pour connaître la syntaxe d'une commande :

    # Commande
    Get-Help <commande> -Detailed

    Get-Help <commande> -Full

    Get-Help <commande> -Examples

    # Script
    Get-Help mon-script.ps1

    Get-Help mon-script.ps1 -Full

    # Défilement de l'aide en ligne page par page
    Get-Help mon-script.ps1 -Full | Out-Host -Paging

Pour tester une commande (mode « dry ») :

    # Test de l'arrêt du système local
    Stop-Computer -WhatIf

Pour confirmer une commande avant exécution :

    # Confirmation de l'arrêt du système local
    Stop-Computer -Confirm

Pour forcer sans confirmer :

    # Force l'arrêt du système local
    Stop-Computer -Confirm:$False

Pour exécuter un script avec privilège, il faut ouvrir une fenêtre PowerShell en tant qu'administrateur puis lancer le script avec `&` devant.

    & "C:\Users\username\mon-script.ps1"

Opérateurs de comparaison :

```powershell
 -eq             Equal
 -ne             Not equal
 -ge             Greater than or equal
 -gt             Greater than
 -lt             Less than
 -le             Less than or equal
 -like           Wildcard comparison
 -notlike        Wildcard comparison
 -match          Regular expression comparison
 -notmatch       Regular expression comparison
 -replace        Replace operator
 -contains       Containment operator
 -notcontains    Containment operator
 -in             Like –contains, but with the operands reversed.(PowerShell 3.0)
 -notin          Like –notcontains, but with the operands reversed.(PowerShell 3.0)
```

### Variables

    [Environment]::GetFolderPath("Desktop")

    [Environment]::GetFolderPath("CommonDesktopDirectory")

    # exemple
    Get-ChildItem([Environment]::GetFolderPath("Desktop"))

    Mode                LastWriteTime         Length Name
    ----                -------------         ------ ----
    -a----       06/12/2019     12:01            985 En cours.lnk
    -a----       06/12/2019     12:01           1136 raccourci.lnk
    -a----       14/02/2020     15:42       32086985 xvm-3.1.jar

Plus de détail sur le site de Microsoft : [Environment.SpecialFolder
Enum](https://docs.microsoft.com/en-us/dotnet/api/system.environment.specialfolder?view=netframework-4.8)

### Couleur

Pour utiliser une couleur en sortie de script :

    Write-Host "Whole text is in green" -ForegroundColor Green
    Write-Host "Whole text is in red" -ForegroundColor Red
    Write-Host "Whole text is in white" -ForegroundColor White

    Write-Host "Whole text is in red with background Yellow" -ForegroundColor Red -BackgroundColor Yellow
    Write-Host "Whole text is in yellow with background Dark Green" -ForegroundColor Yellow -BackgroundColor DarkGreen

![](/powershell-colors.png)

### Historique

    Get-History | Format-List -Property *

Recherche via "CTRL+R" (Reverse Search) ou début de la commande puis "F8"

Suppression de l'historique des commandes de la session en cours :

    Clear-History

Suppression de tout l'historique :

    Remove-Item (Get-PSReadlineOption).HistorySavePath

### PSCredentials

*PSCredential permet de fournir un identifiant et mot de passe dans un script sans interaction.*

<d-alert>

`PSCredential` n\'est pas forcement un compte windows.

</d-alert>

Pour mettre un couple utilisateur / mot de passe en mémoire :

    $cred = Get-Credential -Message "Mot de passe " -UserName DOMAIN\slanteri

    $cred
    UserName                                          Password
    --------                                          --------
    DOMAIN\slanteri System.Security.SecureString

Il est possible de stocker ces identifiants sur le disque pour une utilisation ultérieure :

    $Cred.Password | ConvertFrom-SecureString | Out-File $env:UserProfile\password.txt

Le fichier généré est crypté et il n'est possible de
l'ouvrir que si on a accés au répertoire où le fichier est stocké.

Pour appeler ce fichier afin de réutiliser les identifiants :

    $username = "DOMAIN\slanteri"
    $password = Get-Content $env:UserProfile\password.txt | ConvertTo-SecureString
    $cred = New-Object System.Management.Automation.PSCredential -ArgumentList $username,$password

    $cred
    UserName                                          Password
    --------                                          --------
    DOMAIN\slanteri System.Security.SecureString

Il est possible d'exporter ces informations dans un fichier. Ce fichier pouvant être utilisé dans un script :

    Get-Credential -message "Mot de passe " -UserName DOMAIN\slanteri | Export-Clixml -Path "c:\user.xml"
    $cred = Import-Clixml -Path "c:\user.xml"

    $cred

    UserName                                          Password
    --------                                          --------
    DOMAIN\slanteri System.Security.SecureString

### Divers

Pour trier un résultat :

    Get-xxx | Select-Object Name | Sort-Object








