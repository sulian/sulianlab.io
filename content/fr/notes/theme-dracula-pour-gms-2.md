---
title: Thème "Dracula" pour GameMaker Studio 2
description: 'Thème "Dracula" pour GameMaker Studio 2'
category: Notes
createdAt: 2020-04-20
keywords: thème dracula gamemaker
position: 9
---

Si vous utilisez **vscode**, l’éditeur gratuit de Microsoft, peut-être que vous avez appliqué le thème “[Dracula](https://marketplace.visualstudio.com/items?itemName=dracula-theme.theme-dracula)” ? (et vous avez raison car il est très sympa)

![Aperçu du thème dracula](https://raw.githubusercontent.com/dracula/visual-studio-code/master/screenshot.png)

Vous serez donc content (comme moi) d’apprendre que ce thème existe aussi pour GameMaker Studio 2.

Pour le télécharger et l’installer, ça se passe par [là](https://draculatheme.com/gamemaker-studio/).

![Aperçu du thème dracula dans GMS2](https://draculatheme.com/static/img/screenshots/gamemaker-studio.png)

J’aime beaucoup les interfaces sombres mais pas vraiment celle de GameMaker, ce thème là est beaucoup plus intéressant.