---
title: Kubernetes
description: "Mémo sur kubernetes"
category: Notes
createdAt: 2020-09-20
slug: using-kubernetes
keywords: kubernetes windows windows10
position: 6
---

## Introduction

![](https://d33wubrfki0l68.cloudfront.net/5cb72d407cbe2755e581b6de757e0d81760d5b86/a9df9/docs/tutorials/kubernetes-basics/public/images/module_03_nodes.svg)

## Commandes simples

`kubectl version`

### Lister les ressources

Liste des orchestrateurs :

`kubectl get services`

Liste des noeuds :

`kubectl get nodes`

Pour obtenir les pods en cours d'exécution :

`kubectl get pods`

Informations détaillées :

`kubectl get pods -o wide`

`kubectl config get-contexts`

Vérification de la configuration du noeud : 

`kubectl config view`

```
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: DATA+OMITTED
    server: https://kubernetes.docker.internal:6443
  name: docker-desktop
contexts:
- context:
    cluster: docker-desktop
    user: docker-desktop
  name: docker-desktop
current-context: docker-desktop
kind: Config
preferences: {}
users:
- name: docker-desktop
  user:
    client-certificate-data: REDACTED
    client-key-data: REDACTED
```

Création d'un pod depuis un fichier *yaml* :

`kubectl apply -f C:\Chemin\vers\fichier\yaml\web.yml`

::: tip Astuce
On ne charge dans le noeud kubernetes que des images finalisées ; il est cependant possible de mettre à jour l'application depuis kubernetes.
:::

Vérification du pod :

`kubectl describe po/myPod`

Obtenir des détails sur le processus principal :

`kubectl describe po/myPod -n default`


```
Name:         web
Namespace:    default
Priority:     0
Node:         docker-desktop/192.168.65.3
Start Time:   Sun, 20 Sep 2020 11:17:19 +0200
Labels:       <none>
Annotations:  Status:  Running
IP:           10.1.0.14
IPs:
  IP:  10.1.0.14
Containers:
  nginx:
    Container ID:   docker://51bc5190f1851b50345ff0c60c25fe573bf03f67a5520b7795943e32a9aff929
[...]
Conditions:
  Type              Status
  Initialized       True
  Ready             True
  ContainersReady   True
  PodScheduled      True
Volumes:
  default-token-dlz77:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-dlz77
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type    Reason     Age        From                     Message
  ----    ------     ----       ----                     -------
  Normal  Scheduled  <unknown>  default-scheduler        Successfully assigned default/web to docker-desktop
  Normal  Pulling    88s        kubelet, docker-desktop  Pulling image "nginx:alpine"
  Normal  Pulled     85s        kubelet, docker-desktop  Successfully pulled image "nginx:alpine"
  Normal  Created    84s        kubelet, docker-desktop  Created container nginx
  Normal  Started    84s        kubelet, docker-desktop  Started container nginx
```

Pour les tests, on peut mapper certains ports : 

`kubectl port-forward web 8000:80`

Exécution d'une commande à l'intérieur d'un conteneur (de la même façon que [Docker](using-docker.html)) :

`kubectl exec -it containerID sh`

## Commandes avancées

Visualiser l'activité du pod (et non les journaux d'événements) :

`kubectl logs -f po/myPod`

Attacher un pod et basculer vers le processus principal :

`kubectl attach po/myPod`

Supprimer un pod :

`kubectl delete po/myPod`



### Utiliser les namespaces

 * Segmenter un cluster
 * Isoler les ressources

`kubectl create namespace production` (ou *Kind = namespace* dans la description et *namespace: production* dans les metadatas)

`kubectl get namespaces`

`kubectl get pods --namespace=production`

`kubectl get pods --all-namespaces`

### Configurer les applications

Définir les ConfigMaps

### Comprendre les volumes



----

**Sources :**

  * [Viewing Pods and Nodes](https://kubernetes.io/docs/tutorials/kubernetes-basics/explore/explore-intro/)
