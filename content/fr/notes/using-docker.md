---
title: Docker
description: Mémo sur docker
category: Notes
createdAt: 2018-01-12
keywords: docker windows windows10
position: 4
---

## Introduction

`docker --help | more`

Cycle de vie de base :

* Créer le conteneur à partir d'une image
* Exécuter le conteneur avec un processus spécifié
* Détruire le contenuer une fois le processus terminé 

Cycle de vie avancée :

* Créer le conteneur à partir d'une image
* Exécuter le conteneur avec un service spécifié
* Interagir avec le service
* Arrêt et redémarrage possible du conteneur

## Recherche des images sur Docker Hub

Une recherche standard s'effectue par la commande `docker search`. Si on souhaite avoir la description de l'image, on utilise `--no-trunc` :

`docker search --no-trunc nginx`


## Aborder les conteneurs

### Exécuter un conteneur

`docker run -it ubuntu` (commande par défaut de l'image)

`docker run -it ubuntu ps`

`docker run -it ubuntu bash`

### Exécuter en mode attaché et détaché

`docker run --name=nom1 --hostname=host01 -it ubuntu`

`docker run -d -it centos ping 127.0.0.1 (d = mode détaché)`

`docker ps`

`docker logs containerID`

`docker attach containerID`

### Autres modes d'éxécution

`docker run -d -P nginx`

`docker ps` pour connaitre le mapping des ports

`docker stop containerID`

`docker start containerID`

`docker kill containerID`

`docker exec` pour exécuter un autre processus dans un conteneur, ne pas lancer comme application principale (docker run)

`ps -ef` pour visualiser les processus

### Inspecter un conteneur

`docker inspect containerID`

`docker inspect --format='{{.State.Status}}' containerID`

`docker inspect --format='{{json .State}}' containerID`

### Lister et supprimer des conteneurs

`docker ps`

`docker ps --filter status=exited`

`docker ps --filter exited=137`

exemple : suppression des conteneurs arretés
`docker rm $(docker ps -aq --filter status=exited)`

## Employer les images docker

Espace de nommage :

  * racine (root) : ex. **ubuntu:20.04**
  * utilisateur ou organisation : ex. **masociete/monappli:1.0**
  * autohebergé : **registry.masociete.com;5000/monappli**

Visualiser les différences :

`docker diff containerID`

Valdier les modifications

`docker commit containerID masociete\monos:1.0`

`docker images`

`docker history masociete\monos:1.0`

## Créer une image à partir d'un Dockerfile

`docker build -t masociete/monos:1.0 .`

`Get-Content Dockerfile | docker build -`

Chaque modification du fichier Dockerfile permet de générer une version à jour, les modifications sont visible via `docker history`


Pour tagger une image

`docker tag containerID sulian/test:1.0`

`docker tag containerID sulian/test:latest`

### Bonnes pratiques dans un Dockerfile

*(i.e visiblité et performance)*

  * Chaque ligne crée une nouvelle couche
  * Equilibre entre la lisibilité et le nombre de couches
  * Un ENTRYPOINT par Dockerfile
  * Combiner les commandes avec " && " et " \ "

## Apréhender les volumes

### Générer des volumes

```bash
docker volume create vol1

docker volume ls

docker run -it -v vol1:/www/html centos bash
```

de façon anonyme :
```bash
docker run -it -v vol1:/www/html centos bash
```

`docker volume inspect vol1`

Monter un fichier :

`docker run -d --name=nginx2 -v /home/user1/nginx/nginx.conf:/etc/nginx/nginx.conf`

### Partager les volumes

Astuce : 
Si `docker run -d --name=host1 it <image> bash`, on peut rentrer dans le conteneur avec `docker attach host1`

`-d` = `detach`

### Créer et partager un conteneur de données

`docker run --name=datas -v /data busybox true`

Va créer un volume /data que l'on réutilise dans un autre conteneur en passant le nom du conteneur

`docker run -it --volumes-from datas --name=host1 centos bash`

## Mettre en réseau

-> Pas d'adresse réseau publique

Installer le package `bridge-utils` pour la commande `brctrl` afin de voir les interfaces de Docker.

`brctl show addrIP`

`docker network ls`

 * bridge = pont entre conteneurs
 * host = pile réseau de l'hôte
 * none/null = pas de pile réseau

docker network create --driver bridge --subnet 10.1.0.0/16 --gateay 10.1.0.1 prod

docker network create --driver bridge --subnet 10.2.0.0/16 --gateay 10.2.0.1 dev

docker network inspect prod

docker run -d -it --net prod --name=cont1 --hostname=host1 alpine

docker run -d -it --net prod --name=cont2 --hostname=host2 alpine

La résolution de nom ne fonctionne que sur les noms des conteneurs

Pas de résolution de nom sur le réseau par défaut (sans *--subnet*)

`docker network connect prod host3` pour connecter une machine à un réseau

