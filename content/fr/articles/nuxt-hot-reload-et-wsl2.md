---
title: Nuxt, "hot reload" et WSL 2
description: "Nuxt : résoudre les problèmes de rechargement à chaud sous WSL 2"
category: Articles
createdAt: 2020-09-29
keywords : nuxt nuxtjs hot-reload windows-10 wsl wsl-2
position: 2
---

## Un rechargement à chaud… qui ne fonctionne pas

Pour les besoins de ce projet, j'ai décidé d'utiliser [Nuxt](https://nuxtjs.org/), le framework basé sur [Vuejs](https://vuejs.org/). En général, j'utilise directement VSCode depuis ma session Windows mais en ce moment, je travaille beaucoup avec WSL 2 et machinalement, j'ai lancé une session de développement depuis WSL (donc en initialisant une session à distance).

![](/nuxt-hot-reload-et-wsl2-01.png "Session distante avec VSCode")

Comme d'habitude, je lance le serveur de développement (`yarn dev`) et je commence à mettre à jour mon contenu. Problème : mon site ne s'actualise plus.

## Un soucis avec WSL 2, peut-être ?

Après avoir mis à jour les dépendances de mon projet et vérifié qu'il n'y a pas de soucis avec le projet (simplement en l'exécutant à nouveau depuis Windows). J'ai effectué une petite recherche sur internet (avec les bons mots-clefs) et cela m'a permis de trouver la réponse.

Il s'agit d'un problème avec le chemin d'accès utilisé qui est partagé entre Windows et la sous-couche Linux (cela concerne surtout WSL dans sa version **2**). Pour le résoudre, il faut lancer VSCode (et donc Nuxt) avec un chemin d'accès exclusif à Linux (comme `home/users/sulian`).

A noter que ce bug est ouvert depuis le 19 avril 2019 et n'est toujours pas résolu à ce jour (29 septembre 2020 😮)

<alert type="info">

Depuis le terminal de Windows, la commande `code .` permet de lancer VSCode depuis le répertoire courant. N'étant pas un fan du terminal à la base (avant de me mettre sérieusement à Linux), on ne pense pas forcément à ses petits raccourcis.

</alert>

----

**Sources :**

1. <https://github.com/nuxt/nuxt.js/issues/5550#issuecomment-653906083>
