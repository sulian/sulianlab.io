---
title: Ajout d'une imprimante avec PowerShell
description: "Ajouter une imprimante sous Windows 10 avec PowerShell"
category: 'Articles'
createdAt: 2020-07-14
slug: ajout-imprimante-avec-powershell
keywords: powershell windows-10 imprimante
position: 5
---

## Introduction

Même s'il est possible d'ajouter une imprimante avec une GPO, il est parfois nécessaire d'ajouter manuellement une imprimante lors de l'installation d'un nouveau poste de travail (ou plus tard quand on se rend compte que le profil de l'utilisateur a été mal défini...). Cette tâche peut-être accomplie avec quelques commandes PowerShell. Et ce, à distance, sans avoir besoin de se connecter physiquement sur le poste de travail de l'utilisateur.

Cependant, il faut que le pilote de l'imprimante soit dans le magasin de Windows sinon la commande va échouer. Et finalement, une fois que le pilote est disponible dans le magasin, le reste de la procédure est très simple à suivre.

<alert type="warning">
En effet, si nous n'ajoutons pas le pilote au magasin, l'ajout d'une nouvelle imprimante va échouer quand bien même nous indiquons un chemin d'accès vers le pilote lors de l'installation.
</alert>

## Récupération des pilotes Xerox 64bits

Dans notre exemple, nous allons installer les pilotes 64bits d'une Xerox WorkCentre 7535. Les pilotes sont disponibles à cet emplacement :

https://www.support.xerox.com/support/workcentre-7545-7556/downloads/enus.html?associatedProduct=XRIP_WC7525_base&operatingSystem=win10x64&fileLanguage=fr

Une fois le fichier zip téléchargé, nous allons le décompresser dans un répertoire et utiliser `xcopy` poour copier les fichiers sur le disque du poste distant.

`xcopy C:\XeroxWorkCentre7755 \\pc2.local\c$\XeroxWorkCentre7755 /E /H /S`

<alert type="info">
Il faut être administrateur de domaine et ouvrir une fenêtre du terminal en tant qu'administrateur pour ouvrir une connexion distante avec un autre poste de travail Windows.
</alert>

![Executer en tant qu'administrateur](/powershell/executer-en-tant-qu-administrateur.png)

## Ajout au magasin

Ceci fait, il faut ajouter le pilote au magasin de Windows, c'est l'étape la plus importante. J'utilise `pnputil` parce que c'est l'outil le plus simple mais nous aurions pû utiliser `Add-WindowsDriver` :

``` powershell
Invoke-Command -ComputerName pc2.local -Credential $Cred -ScriptBlock \
{ pnputil.exe -i -a 'c:\XeroxWorkCentre75XX\*.inf' }
```

Après exécution, la commande doit renvoyer quelque chose de similaire à cela :

![Résultat de la commande pnputil](/powershell/ajout-pilote-magasin-windows.png)

## Installation du pilote

Maintenant que le pilote a été ajouté au magasin, nous pouvons installer le pilote, toujours sur le poste distant.

``` powershell
Add-PrinterDriver -ComputerName pc2.local -Name "Xerox WorkCentre 7535 V4 PS"
```

Comment savoir que le nom du pilote est **Xerox WorkCentre 7535 V4 PS** ? Il faut regarder le contenu du fichier *XeroxWorkCentre75XX_PS.inf* : le nom du pilote figure dans le descriptif du fichier à la section **Model**.

![Fichier XeroxWorkCentre75XX_PS.inf](/powershell/description-fichier-inf.png)

## Installation de l'imprimante

### Port réseau

Nous commençons d'abord par déclarer le port réseau que l'imprimante va utiliser, nous utilisons le nom DNS de l'imprimante :

``` powershell
Add-PrinterPort -ComputerName pc2.local -Name "imp01-port" `
-PrinterHostAddress "imp01.local"
```

<alert type="info">
Bien sûr, cette opération n'est à faire que si le port réseau n'existe pas 😉.
</alert>

### Ajout de l'imprimante

Il est temps d'installer l'imprimante. Pour cela, nous devons spécifier tous les éléments que nous avons déjà traités : pc distant, pilote d'impression, port réseau.

``` powershell
Add-Printer -ComputerName pc2.local -Name "imp01" `
-DriverName "Xerox WorkCentre 7535 V4 PS" -portName "imp01-port"
```

### Configuration de l'imprimante

Par défaut, le papier est au format américain (B4 si ma mémoire est bonne), il faut donc indiquer à Windows que le format de papier est le A4 :

``` powershell
Set-PrintConfiguration -ComputerName pc2.local -PrinterName "imp01" -paperSize A4
```

## Conclusion

En utilisant `Get-Printer -ComputerName pc2.local`, nous allons lister les imprimantes installées sur le poste de travail distant et l'imprimante Xerox apparait bien.

![Liste des imprimantes](/powershell/liste-imprimantes.png)

Ces commandes peuvent être reprise dans le cadre d'un script PowerShell plus complet mais aussi dans Ansible dans le cadre d'une automaisation d'un parc informatique : les possibilités sont multiples.