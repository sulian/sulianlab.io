---
title: Migration d'un LDAP vers un AD sous Macos X
description: "Migration d'un LDAP vers un AD sous Macos X"
category: 'Articles'
createdAt: 2020-04-15
keywords: macos ldap "active directory" ad migration
position: 3
---

Un article assez long qui explique comment changer la base d'authentification des utilisateurs sous macosx d'un annuaire LDAP vers un annuaire Active Directory sans perdre les données des utilisateurs locaux.

## Modifier la source d'authentification

<alert type="warning">

Sans mention contraire, les manipulations s'effectuent depuis une session administrateur.

</alert>

### Connexion à l'annuaire « AD »

Se rendre dans les préférences du système puis cliquer sur "**Utilisateurs & groupes**" ; cliquer ensuite sur le cadenas pour déverrouiller la fenêtre (obligatoire pour la suite)

Cliquer sur **Options** puis sur le bouton **Modifier** qui se trouve à coté du "compte serveur réseau".

![Migration LDAP vers AD - 1](/migration-ldap-ad/migration-ldap-ad-macos-01.png)

Pour le moment, il n'y a que l'annuaire LDAP. Cliquer sur le "**+**" pour en ajouter un nouveau.

Saisir l'adresse de votre contrôleur de domaine (ex. ad.domain.com), macos x détecte automatiquement les réglages et propose d'ajouter le poste de travail à l'AD.

![Migration LDAP vers AD - 2](/migration-ldap-ad/migration-ldap-ad-macos-02.png)

<alert type="warning">

L'authentification est obligatoire pour interroger un annuaire Active Directory

</alert>

### Configuration de l'annuaire « AD »

Il y a maintenant un deuxième annuaire disponible pour s'authentifier sur ce poste de travail. Cliquer sur **Ouvrir Utilitaire d'annuaire** pour modifier les options de connexion.

Déverrouiller la fenêtre en cliquant sur le cadenas puis double-cliquer pour éditer le service Active Directory

![Migration LDAP vers AD - 3](/migration-ldap-ad/migration-ldap-ad-macos-05.png)

Dans l'onglet *Expérience utilisateur*, il faut cocher "Créer un compte mobile […]" ; il n'est pas besoin de cocher "Exiger une confirmation […]"

![Migration LDAP vers AD - 4](/migration-ldap-ad/migration-ldap-ad-macos-06.png)

Puis dans l'onglet *Administratif*, il faut cocher "Permettre l'administration par :" et ajouter le groupe **Administrateur du domaine** voir **Administrateurs d'entreprise**. 

![Migration LDAP vers AD - 5](/migration-ldap-ad/migration-ldap-ad-macos-07.png)

Il y a maintenant deux annuaires d'authentification, il n'est pas besoin de modifier l'ordre des domaines car la source LDAP va être supprimée.

![Migration LDAP vers AD - 6](/migration-ldap-ad/migration-ldap-ad-macos-03.png)

## Déconnexion de l'annuaire « LDAP »

Cliquez sur l'annuaire nommé "Serveur Open Directory" pour le sélectionner puis sur le bouton "**-**" sur le supprimer.

![Migration LDAP vers AD - 7](/migration-ldap-ad/migration-ldap-ad-macos-09.png)

Une confirmation s'affiche alors, valider et il n'y a plus qu'un seul annuaire maintenant.

## Migration d'un utilisateur

### Suppression du compte

Toujours dans les préférences du système dans "**Utilisateurs & groupes**" ; cliquer sur l'utilisateur que vous souhaitez migrer et supprimer le en veillant bien à **Ne pas modifier le dossier de départ**.

![Migration LDAP vers AD - 8](/migration-ldap-ad/migration-ldap-ad-macos-11.png)

Vous pouvez ensuite fermer cette fenêtre.

### Modification des autorisations au dossier

Puisque le dossier de démarrage de l'utilisateur est toujours présent, nous allons indiquer indirectement à macos x d'utiliser ce dossier lors d'une authentification avec l'annuaire Active Directory.

<alert type="warning">

Les manipulations suivantes s'effectue depuis le Terminal (dans **Applications** → **Utilitaires**).

</alert>

Il faut modifier les autorisations du dossier de l'utilisateur (« Home ») pour qu'il n'y ait pas de problème lors de la ré-ouverture de session avec le nouvel AD.

Le dossier de l'utilisateur doit correspondre à l'identifiant de cet utilisateur dans l'AD (ex. utilisateur *Prénom Nom*, identifiant *pnom* / dossier *pnom*) ; sans cela, ça ne marche pas.

Pour le vérifier, vous pouvez naviguer dans le dossier "Utilisateurs". Il faut noter que les lignes de commandes font référence à « Users », c'est normal : macos x traduit le nom des dossiers spéciaux au moment de l'affichage.

Lors de la suppression d'un utilisateur, certaines versions de macos x ajoutent un suffixe « (Supprimé) » après le nom de l'utilisateur ; selon les paramètres du serveur LDAP, parfois le nom du dossier n'est pas l'identifiant de l'utilisateur sourtout si ce dernier a plusieurs alias ex. identifiant *pnom*, alias *nom* et dossier *nom*.

<alert type="warning">

Sans `sudo`, point de salut, aucune commande ne marchera. A ne pas oublier donc.

Ne pas oublier l'autocomplétion dans le terminal, il suffit d'appuyer sur la touche **TAB**.

</alert>

Il faut donc renommer le dossier, cela passe par la commande suivante ; attention toutefois aux barres obliques inversées :

`sudo mv /Users/pnom\ \(Supprimé\)/ /Users/pnom`

Il faut ensuite modifier les autorisations du dossier de départ, notamment en donnant les accès au groupe "utilisateurs du domaine" :

`sudo chown pnom:"domaine-ad\Utilisa. du domaine" /Users/pnom/`

Puis modifier les autorisations de toutes les données de l'utilisateur ; cette opération prend du temps (de 5 minutes à 30 minutes selon le volume de données) :

`sudo chown -R pnom /Users/pnom/`

<alert type="warning">

Ne pas oublier le `/` à la fin des chemins d'accès

le `-R` : indique une modification récursive

</alert>

Ceci fait, nous pouvons fermer la session de l'administrateur et ouvrir une session avec le compte de l'utilisateur migré.

Lors de l'ouverture de session, à la question demandant de mettre à jour le trousseau d'accès, répondre "Créer un trousseau", il peut en effet y avoir des problèmes si l'utilisateur utilise son ancien trousseau.

Si le mot de passe de l'utilisateur entre LDAP et AD est le même, macos x ne propose pas de modifier le trousseau (et il n'y a pas de problème non plus).

----

**Sources :**

  * <https://www.jamf.com/jamf-nation/discussions/14417/migrating-from-open-dirctory-to-active-directory> (article de 2015)
  * <https://github.com/pereljon/dsMigrateClient> (article de 2015)
