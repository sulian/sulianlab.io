---
title: Oh my zsh pour WSL2
description: "Installation du terminal zsh sous WSL 2"
category: Articles
createdAt: 2020-09-15
keywords: zsh oh-my-zsh windows-10 wsl wsl-2
position: 1
---

**[Oh My Zsh](https://ohmyz.sh/)** est un framework pour **zsh** qu'il est tout à fait possible d'utiliser sur le terminal WSL de votre Windows 10. Voici les notes que j'ai rassemblées à ce sujet.

## Installation de zsh

Sur une Debian, zsh n'est pas installé par défaut, on commence donc par le faire via `apt install zsh -y`

![](/oh-my-zsh/oh-my-zsh-01.png)

Pour vérifier la version installée, `zsh --version`

## Installation de Oh My Zsh

Oh-my-zsh s'installe avec un script qui va configurer le terminal en indiquant notamment que zsh sera le shell par défaut.

Selon que vous ayez *curl* ou *wget*, la commande diffère légérement, pour ma part, j'utilise curl :

`sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"`

Dorénavant, la configuration de votre shell passera par l'édition du fichier `~/.zshrc`.

![](/oh-my-zsh/oh-my-zsh-02.png)

### Configuration de Oh My Zsh

Nous allons éditer le fichier de configuration avec l'éditeur de texte de votre choix (ici *nano*) et modifier la ligne `ZSH_THEME="agnoster"` pour spécifier le thème `Agnoster` qui est l'un des thèmes le efficient. Comme indiqué dans le fichier, la liste des thèmes est disponible sur [github](https://github.com/ohmyzsh/wiki/Themes)

Il y a d'autres options, je vous conseille de lire la documentation ou d'aller sur [stackoverflow](https://stackoverflow.com/questions/tagged/oh-my-zsh) en cas de problème.

![](/oh-my-zsh/oh-my-zsh-03.png)

Si on redémarre le terminal, on voit que le shell zsh et le thème `Agnoster` sont activés mais il y a un soucis au niveau de l'affichage :

![](/oh-my-zsh/oh-my-zsh-04.png)

## Windows Terminal

Il manque donc une police pour *zsh* mais laquelle ? après quelques recherches, il s'avère que ce problème est solutionné en installant les polices de caratères pour l'outil [Powerline](https://github.com/powerline/fonts/), un module pour vim et pour d'autres applications comme zsh justement.

Sauf que je n'ai pas envie d'installer toutes les polices comme le suggérent certains sites mais uniquement d'installer la police manquante. Dans le cas de Oh My Zsh, c'est la police `DejaVu Sans Mono for Powerline` qui est requise (comme indiqué [ici](https://blog.nillsf.com/index.php/2020/02/17/setting-up-wsl2-windows-terminal-and-oh-my-zsh/)). Après avoir téléchargé l'archive du projet, il faut n'installer que cette famille de police (`Installer pour tous les utilisateurs`).

![](/oh-my-zsh/oh-my-zsh-08.png)

Et indiquer ensuite dans le fichier de configuration de Windows Terminal que l'on souhaite utiliser la polie DejaVu Sans Mono.

![](/oh-my-zsh/oh-my-zsh-05.png)

Cette fois, le shell s'affiche sans problème

![](/oh-my-zsh/oh-my-zsh-06.png)

## Shell Debian (ou autre)

Si dans le nouveau terminal de Windows, l'affichage est correct, cela n'est pas le cas si on lance directement Debian pour WSL. Autant dire que je ne l'utilise que rarement, c'est vraiment facultatif mais autant faire bien les choses.

![](/oh-my-zsh/oh-my-zsh-09.png)

Dans les propriétés de la fenêtre, il faut maintenant choisir la police DejaVu Sans Mono que nous venons d'installer.

![](/oh-my-zsh/oh-my-zsh-10.png)

Le shell s'affiche sans problème

![](/oh-my-zsh/oh-my-zsh-11.png)

## VSCode

Dernier élément à configurer, VSCode, l'éditeur de code de Microsoft. En effet, son terminal aussi n'affiche pas correctement Oh My Zsh et il faut donc lui préciser quelle police il faut utiliser.

Dans les paramètres de VSCode, il faut chercher le paramètre `terminal.integrated.fontFamily`. Ce paramètre peut être modifier à plusieurs endroits, j'ai choisi `Distant [WSL: Debian]` pour ne pas modifier les polices des autres shells (powershell, cmd, ...).

Immédiatement après, la fenêtre de mon terminal se met à jour.

![](/oh-my-zsh/oh-my-zsh-07.png)

----

**Sources :**

1. [Powerline - Overview](https://powerline.readthedocs.io/en/master/overview.html)
1. [WSL2, zsh, and docker. Linux through Windows.](https://nickymeuleman.netlify.app/blog/linux-on-windows-wsl2-zsh-docker)
1. [Setting up WSL2, Windows Terminal and oh-my-zsh](https://blog.nillsf.com/index.php/2020/02/17/setting-up-wsl2-windows-terminal-and-oh-my-zsh/)