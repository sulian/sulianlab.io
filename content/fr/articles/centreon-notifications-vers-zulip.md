---
title: Notifications Centreon vers Zulip
description: Afficher les notifications de Centreon dans Zulip
category: Articles
createdAt: 2020-04-15
keywords: centreon zulip notification
position: 4
---

Centreon possède plusieurs modes de notifications parmis lesquels l'envoi d'un courriel ou d'un SMS. Il est aussi possible avec une ligne de commande (comme `curl` par exemple) d'envoyer des informations via une API.

Pendant les heures ouvrés, il peut être intéressant de remplacer les notifications par courriel (rappel : une notification = un courriel x le nombre de destinataires) par une notification vers une messagerie instantanée comme [zulip](https://zulipchat.com/) en utilisant un bot.

## Configuration du bot

![Commandes de notification](/centreon-to-zulip/centreon-to-zulip-3.png)

![Commandes de notification](/centreon-to-zulip/centreon-to-zulip-4.png)

## Configuration de la commande Centreon

Zulip propose une API très complète : pour émettre un message vers Zulip depuis Centreon, nous utilisons cette API et l'outil cURL qui est disponible depuis n'importe quelle distribution Linux (la documentation de la fonction `send-message` se trouve à cette [adresse](https://www.zulipchat.com/api/send-message)).

La commande est la suivante, nous ne faisons que reprendre l'exemple `curl` depuis la documentation de Zulip :

```bash
curl -skX POST https://mon-serveur-zulip/api/v1/messages \
-u centreon-bot@mon-serveur-zulip:clef-api -d "type=stream" \
-d "to=Centreon" -d "subject=Notifications" \ 
-d "content=$DATE$ - $TIME$ / $NOTIFICATIONTYPE$ : $HOSTNAME$ ($HOSTADDRESS$) est $HOSTSTATE$" 
```

Parmi les points à noter dans cette commande :
1. l'utilisation de l'option `s`, il s'agit du mode silencieux,
1. l'utilisation de l'option `k`, nous utilisons *https* mais le serveur a émis son propre certificat, il s'agit donc ici d'utiliser une connexion non sécurisée,
1. l'utilisation de l'option `X`, nous spécifions ici l'utilisation de POST - ce qui est logique vu que nous poussons des données vers le serveur,
1. comme indiqué dans la documentation, l'API requiert un utilisateur (un bot généralement et une clef d'API),
1. nous indiquons ensuite dans les données le type de canal (*stream/private*), le nom du canal (*"to=centreon"*) et le sujet de la discussion (*"subject=Notifications"*),
1. enfin, les éléments propres à Centreon dans *content*, l'utilisation des variables *$VARIABLE$* qui indique ci et là, le nom de l'hôte ou du service et la description de l'évenement.

![Commande Centreon](/centreon-to-zulip/centreon-to-zulip-1.png)

## Configuration de l'envoi des notifications

![Commandes de notification](/centreon-to-zulip/centreon-to-zulip-2.png)

## Aperçu du résultat

![Commandes de notification](/centreon-to-zulip/centreon-to-zulip-5.png)