---
title: Introduction
description: Diary of a developpeur / sysadmin
position: 1
category: ''
version: 1.0
fullscreen: false
contents:
  - Automatisation avec Ansible (& PowerShell aussi)
  - Conteneurisation avec Docker & Kubernetes
  - Gestion de projet
  - Gestion documentaire avec Qualios
  - PowerShell ‣ script, administration, automatisation
  - Supervision avec Centreon
  - Virtualisation avec VMware
---

## Présentation

Tout au long de ma carrière, j'ai travaillé sur différents projets / os / logiciels / matériels… et il (m')est impossible de tout retenir. Ce site regroupe toutes les notes prises à travers mon travail d'analyste / développeur & administrateur système.

A l'origine, ces notes sont stockées sur un wiki d'entreprise que j'ai mis en place ; comme il n'est pas accessible depuis l'extérieur, j'ai créé ce carnet de note pour que tout le monde puisse en profiter.

## Contenu

Mon carnet de notes aborde les thèmes suivants :

<list :items="contents"></list>

## Mise à jour du contenu

Si vous découvrez une coquille sur un article, n'hésitez pas à ouvrir un ticket sur gitlab afin que je corrige l'article rapidement.
