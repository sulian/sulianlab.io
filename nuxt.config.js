import theme from '@nuxt/content-theme-docs'

export default theme ({
    server: {
        port: 3000, // default: 3000
        // host: '0.0.0.0' // default: localhost
    },  
    loading: {
        color: '#00CD81'
    },
    generate: {
        dir: 'public',
    //     fallback: '404.html', // for Netlify
    //     routes: ['/'] // give the first url to start crawling
    },
    i18n: {
        locales: () => [{
            code: 'fr',
            iso: 'fr-FR',
            file: 'fr-FR.js',
            name: 'Français'
        }, {
            code: 'en',
            iso: 'en-US',
            file: 'en-US.js',
            name: 'English'
        }],
        defaultLocale: 'fr'
    }
})