# Changelog

Tous les changements notables de ce projet sont documentés dans ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/fr/1.0.0/),
et ce projet applique la [gestion sémantique de version](https://semver.org/lang/fr).

## [Unreleased]

----

## [0.10.0] - 2021-03-22

### Added
- article "PowerShell"

### Changed
- migration de theme-content-docs vers Docus
- modification du nom des articles et des  notes

## [0.9.0] - 2020-09-30

### Added
- import des articles depuis le blog précédent
- mise en forme des données d'en-tête (*Front Matter*) pour chaque article

## [0.0.1] - 2020-09-30

### Added 
- première publication pour tests
- mise en place de l'intégration continue