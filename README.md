# Mon carnet de note

## Présentation

Tout au long de ma carrière, j'ai travaillé sur différents projets / os / logiciels / matériels… et il (m')est impossible de tout retenir. Ce site regroupe toutes les notes prises à travers mon travail d'analyste / développeur & administrateur système.

## Génération du site avec @nuxt/content

Le moteur derrière ce site est [NuxtJS](https://nuxtjs.org/) - "*The Intuitive Vue Framework*" et son module [@content](https://content.nuxtjs.org/) qui sert à la publication de contenu.

### Préparation

Install dependencies:

```bash
yarn install
```

### Développement

```bash
yarn dev
```

### Génération statique

Cela va créer le répertiore `public/` pour la publication sur un hôte statique :

```bash
yarn generate
```

Pour prévisualiser le site statique, lancer `yarn start`

Pour une explicationdétaillée de comment les choses fonctionnent, il faut se rendre sur [nuxt/content](https://content.nuxtjs.org) et [@nuxt/content theme docs](https://content.nuxtjs.org/themes-docs).
